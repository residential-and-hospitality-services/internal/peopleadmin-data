#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define GET_BIT(array, index) ((array[index / 8] & (1 << index % 8)) > 0)
#define SET_BIT(array, index) (array[index / 8] |= (1 << index % 8))
#define CLEAR_BIT(array, index) (array[index / 8] &= ~(1 << index % 8))

#define START_USED_COLUMNS_SIZE 32

int main(int argc, char **argv) {
    FILE *f;
    char *used_columns, c, inside_quote = 0, marked = 0, skip_line = 1;
    short used_columns_size = START_USED_COLUMNS_SIZE, column = 0, first_printed_column = -1;

    if (argc != 2) {
        fputs("No.\n", stderr);
        return 1;
    }

    f = fopen(argv[1], "r");
    if (!f) {
        fputs("Failed to open file.\n", stderr);
        return 1;
    }

    used_columns = malloc(used_columns_size);
    if (!used_columns) {
        fputs("Failed to allocate memory.\n", stderr);
        return 1;
    }
    memset(used_columns, 0, used_columns_size);

    /*
     * Determine which columns contain at least one data value.
     */

    c = getc(f);

    for (;;) {
        if (!inside_quote) {
            if (c == EOF) {
                break;
            } else if (c == '"') {
                inside_quote = 1;
            } else if (c == ',') {
                marked = 0;
                if (column == SHRT_MAX) {
                    fputs("Counter exceeds storage size of variable.\n", stderr);
                    return 1;
                }
                column++;
                if (column / 8 + 1 > used_columns_size) {
                    short previous_size;

                    previous_size = used_columns_size;
                    used_columns_size *= 2;
                    used_columns = realloc(used_columns, used_columns_size);
                    if (!used_columns) {
                        fputs("Failed to allocate memory.\n", stderr);
                        return 1;
                    }
                    memset(used_columns + previous_size, 0, used_columns_size - previous_size);
                }
            } else if (c == '\r') {
                c = getc(f);
                if (c != '\n') {
                    fputs("Malformed record: CR without LF.\n", stderr);
                    return 1;
                }
                continue;
            } else if (c == '\n') {
                marked = 0;
                column = 0;
                skip_line = 0;
            }

            if (!marked && !skip_line && c != ',' && c != '\n') {
                SET_BIT(used_columns, column);
                marked = 1;
                if (first_printed_column == -1) {
                    first_printed_column = column;
#ifdef DEBUG
                    fprintf(stderr, "First printed column: %i\n", first_printed_column);
#endif
                }
            }
        } else {
            if (c == EOF) {
                fputs("Malformed record: no closing quote.\n", stderr);
                return 1;
            } else if (c == '"') {
                c = getc(f);
                if (c != '"') {
                    inside_quote = 0;
                    continue;
                }
            }
        }

        c = getc(f);
    }

#ifdef DEBUG
    fputs("Marked columns:\n", stderr);
    for (column = 0; column < used_columns_size * 8 && column >= 0; column++) {
        if (GET_BIT(used_columns, column)) {
            fprintf(stderr, "  %i\n", column);
        }
    }
#endif

    /*
     * Print columns marked as used.
     */

    if (first_printed_column == -1) {
        return 0;
    }

    rewind(f);
    column = 0;
    marked = GET_BIT(used_columns, 0);
    c = getc(f);

    for (;;) {
        if (!inside_quote) {
            if (c == EOF) {
                break;
            } else if (c == '"') {
                inside_quote = 1;
                if (marked) {
                    putchar('"');
                }
            } else if (c == ',') {
                column++;
                marked = GET_BIT(used_columns, column);
                if (marked && column != first_printed_column) {
                    putchar(',');
                }
            } else if (c == '\r') {
                c = getc(f);
                if (c != '\n') {
                    fputs("Malformed record: CR without LF.\n", stderr);
                    return 1;
                }
                putchar('\r');
                continue;
            } else if (c == '\n') {
                column = 0;
                marked = GET_BIT(used_columns, 0);
                putchar('\n');
            } else if (marked) {
                putchar(c);
            }
        } else {
            if (c == EOF) {
                fputs("Malformed record: no closing quote.\n", stderr);
                return 1;
            } else if (c == '"') {
                c = getc(f);

                if (c == EOF) {
                    inside_quote = 0;
                    if (marked) {
                        putchar('"');
                        putchar('\n');
                    }
                    continue;
                } else if (c == '"') {
                    if (marked) {
                        putchar('"');
                        putchar('"');
                    }
                } else {
                    inside_quote = 0;
                    if (marked) {
                        putchar('"');
                    }
                    continue;
                }
            } else {
                if (marked) {
                    putchar(c);
                }
            }
        }

        c = getc(f);
    }

#ifndef QUICK_AND_DEAD
    fclose(f);
    free(used_columns);
#endif

    return 0;
}
