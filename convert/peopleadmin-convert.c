#include <stdio.h>

int main(int argc, char **argv) {
    char c, inside_quote = 0;

    for (;;) {
        c = getchar();

        loop:
        if (!inside_quote) {
            if (c == EOF) {
                return 0;
            } else if (c == '|' || c == '@') {
            } else if (c == '#') {
                c = getchar();

                if (c == '#') {
                    putchar(',');
                } else {
                    putchar('#');
                    putchar(c);
                }
            } else if (c == '"') {
                inside_quote = 1;
                putchar('"');
            } else if (c == '\\') {
                c = getchar();

                if (c == EOF) {
                    fputs("Malformed record: stray backslash.\n", stderr);
                } else if (c == 'N') {
                    /* Replace nulls (<\N>) with blanks. */
                } else {
                    putchar(c);
                }
            } else {
                putchar(c);
            }
        } else {
            if (c == EOF) {
                fputs("Malformed record: no closing quote.\n", stderr);
                return 1;
            } else if (c == '"') {
                c = getchar();

                if (c == EOF) {
                    /* Field termination at last record in a file without a final newline. */
                    putchar('"');
                    putchar('\n');
                    return 0;
                } else if (c == '"') {
                    putchar('"');
                    putchar('"');
                } else {
                    inside_quote = 0;
                    putchar('"');
                    goto loop;
                }
            } else {
                putchar(c);
            }
        }
    }
}
