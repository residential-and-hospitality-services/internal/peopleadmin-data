#!/usr/bin/env bash

OUTDIR=/srv/peopleadmin
LOCKFILE=/var/run/task.lock

declare tmp=

quit() {
    local r=$?
    trap "" INT QUIT TERM HUP PIPE USR1 USR2
    trap -- EXIT
    [[ -n ${1-} ]] && printf "%s\\n" "$1" >&2
    [[ -n $tmp ]] && rm -rf -- "$tmp"
    if [[ -n ${1-} ]]; then exit 1; else exit $r; fi
}

inode() {
    stat -L -c%i -- "$1"
}

fd_inode() {
    inode "/proc/$$/fd/$1"
}

trap quit EXIT
trap 'quit "Uncaught error at $(basename -- "$0"):$LINENO"' ERR
set -Euo pipefail

if ! [[ -d $OUTDIR && -w $OUTDIR && -x $OUTDIR ]]; then
    quit "Bad output directory $OUTDIR."
fi

for file in /var/run/secrets/{fetch_key,known_hosts,sftp_source}; do
    [[ -r $file ]] || quit "Could not read file $file."
done

exec {lockfd}>$LOCKFILE
flock -n "$lockfd" || quit "Could not lock."
if [[ $(fd_inode "$lockfd") != $(inode "$LOCKFILE") ]]; then
    quit "Stolen lock!"
fi

tmp=$(mktemp -d)
cd -- "$tmp"

sftp -p -i /var/run/secrets/fetch_key \
    -o PreferredAuthentications=publickey \
    -o StrictHostKeyChecking=yes \
    -o UserKnownHostsFile=/var/run/secrets/known_hosts \
    "$(</var/run/secrets/sftp_source)" peopleadmin.zip || quit "sftp failed"

peopleadmin-process -r peopleadmin.zip || quit "peopleadmin-process failed"

mv peopleadmin.zip peopleadmin-reduced.zip "$OUTDIR/"

rm -- "$LOCKFILE"
exec {lockfd}>&-
