ARG DISTRO

FROM ${DISTRO} AS build

RUN apk add -q gcc libc-dev make valgrind bash

COPY convert /usr/src/peopleadmin-convert
WORKDIR /usr/src/peopleadmin-convert

RUN chown -R nobody: . \
    && find . -type f \! -perm /0111 -exec chmod 0644 -- \{\} \; \
    && find . -type f -perm /0111 -exec chmod 0755 -- \{\} \; \
    && find . -type d -exec chmod 0755 -- \{\} \;

USER nobody

RUN make

FROM ${DISTRO} AS final

ARG UID

COPY --from=build --chown=root:root \
    /usr/src/peopleadmin-convert/build/bin /usr/local/bin

RUN apk add -q openssh-client zip bash

RUN echo "0 1 * * * /task" >>/var/spool/cron/crontabs/data
RUN adduser -h /srv/peopleadmin -u "${UID}" -H -D data
RUN chmod 1777 /var/run

COPY fetch/entrypoint fetch/task /
RUN chmod 0755 /entrypoint /task
ENTRYPOINT ["/entrypoint"]
